import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'User Profile';

  profileData:any;
  base_url ='https://api.github.com/users/';
  user_name = 'octocat';
  content_first:boolean = true;
  content_second:boolean = false;
  // public img = new Image();
  url_img: any;
  fileToUpload: File = null;
  width: string;
  size: any;
   constructor(private router: Router,private http:HttpClient){
   }
	ngOnInit() {
     this.http.get<baseResponse>(this.base_url +this.user_name).subscribe((res)=>{
       console.log(res)
       this.profileData= res;
     }
     )
     console.log(this.profileData)
  }
  getSecond(){
    this.content_second = true;
    this.content_first = false;
    this.title = 'Uplode Image'
  }
  getBack(){
    this.content_first= true;
    this.content_second= false;
  }
  onSelectFile(event) { 
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); 
      console.log(reader.readAsDataURL)
      reader.onload = (event) => { 
        console.log(event)
        this.url_img = reader.result;
        console.log(this.url_img)
        let img = new Image();
     img.onload = () => {
      //  let width = this.width;
      console.log(this.width);
       
      if(this.width > '600'){
        alert(' Crop It' )
      }
      else if(this.width <= '600'){
        alert('Uplode Another Image');
      }
      else if(this.width == '600'){
        alert(' this image width is correct width.')
      }
    }
      img.src = this.url_img;
      }
    }
}
handleFileInput(files: FileList) {
  this.fileToUpload = files.item(0);
  console.log(this.fileToUpload)
  this.url_img= this.fileToUpload.name.toString();
  console.log(this.url_img)
}
}

export class baseResponse{
  status: string;
	error_code: string;
	msg: any;
}
